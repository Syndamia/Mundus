﻿namespace Mundus.Data {
    public static class MapSizes {
        //These are the map sizes that are generated
        public const int SMALL = 30;
        public const int MEDIUM = 60;
        public const int LARGE = 100;

        public static int CurrSize { get; set; }
    }
}
