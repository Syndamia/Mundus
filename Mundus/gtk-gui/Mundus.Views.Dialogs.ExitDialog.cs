
// This file has been generated by the GUI designer. Do not modify.
namespace Mundus.Views.Dialogs
{
	public partial class ExitDialog
	{
		private global::Gtk.Label lblMessage;

		private global::Gtk.Button btnCancel;

		private global::Gtk.Button btnSave;

		private global::Gtk.Button btnExit;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget Mundus.Views.Dialogs.ExitDialog
			this.Name = "Mundus.Views.Dialogs.ExitDialog";
			this.Title = "";
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			// Internal child Mundus.Views.Dialogs.ExitDialog.VBox
			global::Gtk.VBox w1 = this.VBox;
			w1.Name = "DExit_VBox";
			w1.BorderWidth = ((uint)(2));
			// Container child DExit_VBox.Gtk.Box+BoxChild
			this.lblMessage = new global::Gtk.Label();
			this.lblMessage.HeightRequest = 50;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.LabelProp = "You haven\'t saved for {number} of seconds. Are you sure you want to exit without " +
				"saving?";
			w1.Add(this.lblMessage);
			global::Gtk.Box.BoxChild w2 = ((global::Gtk.Box.BoxChild)(w1[this.lblMessage]));
			w2.Position = 0;
			w2.Expand = false;
			w2.Fill = false;
			// Internal child Mundus.Views.Dialogs.ExitDialog.ActionArea
			global::Gtk.HButtonBox w3 = this.ActionArea;
			w3.Name = "DExit_ActionArea";
			w3.Spacing = 10;
			w3.BorderWidth = ((uint)(5));
			w3.LayoutStyle = ((global::Gtk.ButtonBoxStyle)(4));
			// Container child DExit_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.btnCancel = new global::Gtk.Button();
			this.btnCancel.CanDefault = true;
			this.btnCancel.CanFocus = true;
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.UseUnderline = true;
			this.btnCancel.Label = "Cancel";
			this.AddActionWidget(this.btnCancel, -6);
			global::Gtk.ButtonBox.ButtonBoxChild w4 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w3[this.btnCancel]));
			w4.Expand = false;
			w4.Fill = false;
			// Container child DExit_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.btnSave = new global::Gtk.Button();
			this.btnSave.CanDefault = true;
			this.btnSave.CanFocus = true;
			this.btnSave.Name = "btnSave";
			this.btnSave.UseUnderline = true;
			this.btnSave.Label = "Save & Exit";
			this.AddActionWidget(this.btnSave, -3);
			global::Gtk.ButtonBox.ButtonBoxChild w5 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w3[this.btnSave]));
			w5.Position = 1;
			w5.Expand = false;
			w5.Fill = false;
			// Container child DExit_ActionArea.Gtk.ButtonBox+ButtonBoxChild
			this.btnExit = new global::Gtk.Button();
			this.btnExit.CanFocus = true;
			this.btnExit.Name = "btnExit";
			this.btnExit.UseUnderline = true;
			this.btnExit.Label = "Exit without saving";
			this.AddActionWidget(this.btnExit, -2);
			global::Gtk.ButtonBox.ButtonBoxChild w6 = ((global::Gtk.ButtonBox.ButtonBoxChild)(w3[this.btnExit]));
			w6.Position = 2;
			w6.Expand = false;
			w6.Fill = false;
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 563;
			this.DefaultHeight = 95;
			this.Show();
			this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
		}
	}
}
