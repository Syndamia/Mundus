
// This file has been generated by the GUI designer. Do not modify.
namespace Mundus.Views.Windows
{
	public partial class LogWindow
	{
		private global::Gtk.Table tbUI;

		private global::Gtk.Button btnNewer;

		private global::Gtk.Button btnOlder;

		private global::Gtk.Label lblLog1;

		private global::Gtk.Label lblLog2;

		private global::Gtk.Label lblLog3;

		private global::Gtk.Label lblLog4;

		private global::Gtk.Label lblLog5;

		private global::Gtk.Label lblLog6;

		private global::Gtk.Label lblLog7;

		private global::Gtk.Label lblLog8;

		private global::Gtk.Label lblLog9;

		protected virtual void Build()
		{
			global::Stetic.Gui.Initialize(this);
			// Widget Mundus.Views.Windows.LogWindow
			this.Name = "Mundus.Views.Windows.LogWindow";
			this.Title = "LogWindow";
			this.WindowPosition = ((global::Gtk.WindowPosition)(4));
			this.Resizable = false;
			this.AllowGrow = false;
			// Container child Mundus.Views.Windows.LogWindow.Gtk.Container+ContainerChild
			this.tbUI = new global::Gtk.Table(((uint)(9)), ((uint)(2)), false);
			this.tbUI.Name = "tbUI";
			// Container child tbUI.Gtk.Table+TableChild
			this.btnNewer = new global::Gtk.Button();
			this.btnNewer.WidthRequest = 60;
			this.btnNewer.HeightRequest = 50;
			this.btnNewer.CanFocus = true;
			this.btnNewer.Name = "btnNewer";
			this.btnNewer.UseUnderline = true;
			this.btnNewer.Label = "Newer";
			this.tbUI.Add(this.btnNewer);
			global::Gtk.Table.TableChild w1 = ((global::Gtk.Table.TableChild)(this.tbUI[this.btnNewer]));
			w1.BottomAttach = ((uint)(4));
			w1.LeftAttach = ((uint)(1));
			w1.RightAttach = ((uint)(2));
			w1.XOptions = ((global::Gtk.AttachOptions)(4));
			w1.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.btnOlder = new global::Gtk.Button();
			this.btnOlder.WidthRequest = 60;
			this.btnOlder.HeightRequest = 50;
			this.btnOlder.CanFocus = true;
			this.btnOlder.Name = "btnOlder";
			this.btnOlder.UseUnderline = true;
			this.btnOlder.Label = "Older";
			this.tbUI.Add(this.btnOlder);
			global::Gtk.Table.TableChild w2 = ((global::Gtk.Table.TableChild)(this.tbUI[this.btnOlder]));
			w2.TopAttach = ((uint)(5));
			w2.BottomAttach = ((uint)(9));
			w2.LeftAttach = ((uint)(1));
			w2.RightAttach = ((uint)(2));
			w2.XOptions = ((global::Gtk.AttachOptions)(4));
			w2.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog1 = new global::Gtk.Label();
			this.lblLog1.WidthRequest = 250;
			this.lblLog1.HeightRequest = 50;
			this.lblLog1.Name = "lblLog1";
			this.lblLog1.Wrap = true;
			this.lblLog1.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog1);
			global::Gtk.Table.TableChild w3 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog1]));
			w3.XOptions = ((global::Gtk.AttachOptions)(4));
			w3.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog2 = new global::Gtk.Label();
			this.lblLog2.WidthRequest = 250;
			this.lblLog2.HeightRequest = 50;
			this.lblLog2.Name = "lblLog2";
			this.lblLog2.Wrap = true;
			this.lblLog2.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog2);
			global::Gtk.Table.TableChild w4 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog2]));
			w4.TopAttach = ((uint)(1));
			w4.BottomAttach = ((uint)(2));
			w4.XOptions = ((global::Gtk.AttachOptions)(4));
			w4.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog3 = new global::Gtk.Label();
			this.lblLog3.WidthRequest = 250;
			this.lblLog3.HeightRequest = 50;
			this.lblLog3.Name = "lblLog3";
			this.lblLog3.Wrap = true;
			this.lblLog3.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog3);
			global::Gtk.Table.TableChild w5 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog3]));
			w5.TopAttach = ((uint)(2));
			w5.BottomAttach = ((uint)(3));
			w5.XOptions = ((global::Gtk.AttachOptions)(4));
			w5.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog4 = new global::Gtk.Label();
			this.lblLog4.WidthRequest = 250;
			this.lblLog4.HeightRequest = 50;
			this.lblLog4.Name = "lblLog4";
			this.lblLog4.Wrap = true;
			this.lblLog4.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog4);
			global::Gtk.Table.TableChild w6 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog4]));
			w6.TopAttach = ((uint)(3));
			w6.BottomAttach = ((uint)(4));
			w6.XOptions = ((global::Gtk.AttachOptions)(4));
			w6.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog5 = new global::Gtk.Label();
			this.lblLog5.WidthRequest = 250;
			this.lblLog5.HeightRequest = 50;
			this.lblLog5.Name = "lblLog5";
			this.lblLog5.Wrap = true;
			this.lblLog5.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog5);
			global::Gtk.Table.TableChild w7 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog5]));
			w7.TopAttach = ((uint)(4));
			w7.BottomAttach = ((uint)(5));
			w7.XOptions = ((global::Gtk.AttachOptions)(4));
			w7.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog6 = new global::Gtk.Label();
			this.lblLog6.WidthRequest = 250;
			this.lblLog6.HeightRequest = 50;
			this.lblLog6.Name = "lblLog6";
			this.lblLog6.Wrap = true;
			this.lblLog6.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog6);
			global::Gtk.Table.TableChild w8 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog6]));
			w8.TopAttach = ((uint)(5));
			w8.BottomAttach = ((uint)(6));
			w8.XOptions = ((global::Gtk.AttachOptions)(4));
			w8.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog7 = new global::Gtk.Label();
			this.lblLog7.WidthRequest = 250;
			this.lblLog7.HeightRequest = 50;
			this.lblLog7.Name = "lblLog7";
			this.lblLog7.Wrap = true;
			this.lblLog7.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog7);
			global::Gtk.Table.TableChild w9 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog7]));
			w9.TopAttach = ((uint)(6));
			w9.BottomAttach = ((uint)(7));
			w9.XOptions = ((global::Gtk.AttachOptions)(4));
			w9.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog8 = new global::Gtk.Label();
			this.lblLog8.WidthRequest = 250;
			this.lblLog8.HeightRequest = 50;
			this.lblLog8.Name = "lblLog8";
			this.lblLog8.Wrap = true;
			this.lblLog8.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog8);
			global::Gtk.Table.TableChild w10 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog8]));
			w10.TopAttach = ((uint)(7));
			w10.BottomAttach = ((uint)(8));
			w10.XOptions = ((global::Gtk.AttachOptions)(4));
			w10.YOptions = ((global::Gtk.AttachOptions)(4));
			// Container child tbUI.Gtk.Table+TableChild
			this.lblLog9 = new global::Gtk.Label();
			this.lblLog9.WidthRequest = 250;
			this.lblLog9.HeightRequest = 50;
			this.lblLog9.Name = "lblLog9";
			this.lblLog9.Wrap = true;
			this.lblLog9.Justify = ((global::Gtk.Justification)(2));
			this.tbUI.Add(this.lblLog9);
			global::Gtk.Table.TableChild w11 = ((global::Gtk.Table.TableChild)(this.tbUI[this.lblLog9]));
			w11.TopAttach = ((uint)(8));
			w11.BottomAttach = ((uint)(9));
			w11.XOptions = ((global::Gtk.AttachOptions)(4));
			w11.YOptions = ((global::Gtk.AttachOptions)(4));
			this.Add(this.tbUI);
			if ((this.Child != null))
			{
				this.Child.ShowAll();
			}
			this.DefaultWidth = 310;
			this.DefaultHeight = 450;
			this.Show();
			this.DeleteEvent += new global::Gtk.DeleteEventHandler(this.OnDeleteEvent);
			this.btnOlder.Clicked += new global::System.EventHandler(this.OnBtnOlderClicked);
			this.btnNewer.Clicked += new global::System.EventHandler(this.OnBtnNewerClicked);
		}
	}
}
