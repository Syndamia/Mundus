# Mundus
**Note: This is the `master` branch, here is only the stuff that I want to be in the project. To see the project that also has implimented exam requirements, see branch [requirements](https://github.com/Syndamia/Mundus/tree/requirements)**

Mundus is a 2D sandbox openworld RPG. You can explore, destroy, create or fight, the world is your oyster. It is built in `C#` and `Gtk# 2.0`.

This is an exam project for IT-kariera Module 7 (Software development) course but I plan on working on the concept long after (as the [Velekurt](https://github.com/Velekurt) project).

For more information, visit [my website](http://www.syndamia.com/projects/mundus/introduction).

## How to play: 
Install the dependencies and run the executable (`.exe` file).

Dependencies:
 - [Gtk-Sharp](https://www.mono-project.com/docs/gui/gtksharp/installer-for-net-framework/)
 - [.NET Framework 4.7](https://dotnet.microsoft.com/download/dotnet-framework/net47)

Supported platforms:
 - Windows
 - Linux (to run it on Linux, you should use [mono](https://www.mono-project.com/download/stable/#download-lin))
 - MacOS (**hasn't been tested, should work in theory**)
